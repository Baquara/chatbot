from flask import Flask, render_template, request, session, redirect, url_for, jsonify
import requests
from rivescript import RiveScript
import re
import json
import uuid
from sqlalchemy import *
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import Session
from funcaux import *
from sqlalchemy.engine import reflection


app = Flask(__name__)
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'

#Caso usar o banco em SQLite ao invés de MySQL
db = create_engine('sqlite:///banco.db')


#Originalmente tinha optado por usar diretamente o banco de dados mysql, mas o SQLAlchemy não se mostrou muito amigável com MySQL... Como não faria uma diferença grande a nível de funcionalidade/uso para este desafio, optei por manter conexão com o SQLite, mas ainda assim, o banco MySQL existe na porta designada quando é executado em modo Dockerizado, inicializado com o que este App necessita.
#db = create_engine('mysql://root:root@localhost:32000/banco')



sqlsession = Session(db)


db.echo = False
metadata = MetaData(db)

Base = automap_base()
Base.prepare(db, reflect=True)

#Clona as tabelas do banco para o App

Historico = Base.classes.historico
Usuarios = Base.classes.usuarios

#inicializa o Rivescript

bot = RiveScript(utf8=True)
bot.unicode_punctuation = re.compile(r'[.,!?;:]')
bot.load_directory("./brain")
bot.sort_replies()

#Pega o ID da sessão

my_id = uuid.uuid1()


#Verifica cada propriedade do usuário da sessão a ser salva

def verificarcadaprop(resp_dict):
    listaprop = ['name','cidade','hobby','idade']
    for x in listaprop:
        try:
            if(resp_dict['localuser'][x]!=None):
                escreveprop(x,resp_dict)
        except:
            pass

#Adiciona as propriedades no banco

def escreveprop(prop,resp_dict):
    truth = sqlsession.query(Usuarios.id).filter_by(id_sessao=str(my_id)).scalar() is None
    coluna=""
    if(prop=='name'):
        session['username']=resp_dict['localuser']['name']
        coluna='nome'
    if(prop=='cidade'):
        coluna='cidade'
    if(prop=='hobby'):
        coluna='hobby'
    if(prop=='idade'):
        coluna='idade'
    if(truth):
        sqlsession.add(Usuarios(id_sessao=str(my_id), nome=str(resp_dict['localuser'][prop])))
        sqlsession.commit()
    else:
        sqlsession.query(Usuarios).filter(Usuarios.id_sessao == str(my_id)).update({coluna: str(resp_dict['localuser'][prop])})
        sqlsession.commit()


#Retorna a view web


@app.route("/")
def home():
    return render_template("index.html")


#API principal da aplicação

@app.route("/get")
def get_bot_response():
    if 'username' in session:
        bot.set_uservar("localuser","name",session['username'])
    userText = request.args.get('msg')
    #se for um número, verifica a função FizzBuzz
    if userText.isnumeric():
        resposta = fizzbuzz(int(userText))
        if resposta!=None:
            return resposta
    resposta_do_bot= str(bot.reply("localuser", userText))
    resp_dict = json.loads(json.dumps(bot.get_uservars()))
    try:
        sqlsession.add(Historico(id_sessao=str(my_id), pergunta=str(resp_dict['localuser']['__history__']['input'][0]),resposta=str(resp_dict['localuser']['__history__']['reply'][0])))
        sqlsession.commit()
        verificarcadaprop(resp_dict)
    except:
        pass
    return resposta_do_bot


if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0')