# Desafio - CHATBOT

Repositório com a solução de um desafio para Chatbot.

Esta solução inclui uma interface gráfica web que pode ser aberta no navegador. Alternativamente, o backend também aceita requisições em CLI.



## Dependências

- Flask
- SQLAlchemy
- requests
- rivescript
- sqlite3-to-mysql

(Caso utilizem Dockerizado)

- Docker

## Como executar

Para executar em modo dockerizado, utiliza-se:

>docker-compose up

E depois, acessar a versão (em interface gráfica) do projeto em:

>http://0.0.0.0:5001/


Se for rodar localmente

>python \_\_init\_\_.py

Onde será executado no localhost na porta 5000.


## Deploy no AWS

Realizei um deploy no Amazon Web Services para esta aplicação.

Está acessível pelo endereço: http://18.217.156.188:5000/

Também disponibilizo o repositório Docker que utilizei:

https://hub.docker.com/repository/docker/feliperfabreu/chatbot


![Preview](/imgs/aws.png)

## Rotas da API

Considerando a versão dockerizada rodando, utiliza-se a rota em GET:

>http://0.0.0.0:5001/get?msg=<MENSAGEM PARA O BOT\>

Que devolverá a mensagem do Bot.


Exemplos usando o link AWS:

http://18.217.156.188:5000/get?msg=como%20vai

http://18.217.156.188:5000/get?msg=15


## Features:

Este bot foi feito em Rivescript, e utiliza uma base que assimila as sentenças dentro de expressões regulares para então fornecer as réplicas.


![Preview](/imgs/logconversa.png)


Neste exemplo, as propriedades do usuário da sessão (nome, hobby, cidade e idade) são salvas no banco de dados, em uma poderosa combinação dos dados de sessão JSONificados do Rivescript e a lógica do backend em Python:



![Preview](/imgs/dadossalvos.png)


A aplicação também salva todo o histórico da conversa, conforme requisitado pelo desafio:


![Preview](/imgs/dadossalvos2.png)


## Testes automatizados:

No diretório /testes , está presente o arquivo testes.py, que pode ser executado usando a biblioteca pytest.

>pytest testes.py

O caminho para as requisições está considerando que a **versão Dockerizada da aplicação está em execução**.

![Preview](/imgs/resultadosteste.png)

Os testes incluem o desafio FizzBuzz:

![Preview](/imgs/fizzbuzz.png)

![Preview](/imgs/fizzbuzz2.png)



## Observações:


Embora um banco de dados MySQL esteja disponível na imagem dockerizada, para este desafio foi optado o uso de um banco de dados em SQLite.

Caso queiram importar os dados para o banco de dados MySQL presente no contêiner Docker, deve-se primeiro fazer:

>mysql --host=127.0.0.1 --port=32000 -u root -p

SENHA: root

>DROP DATABASE banco;

e então rodar, utilizando o sqlite3mysql,

>sqlite3mysql -f ./banco.db -d banco -u root -p root -h localhost -P 32000

![Preview](/imgs/mysql.png)

