import requests


def teste1():
    response = requests.get("http://0.0.0.0:5001/")
    assert response.status_code == 200

def teste2():
    response = requests.get("http://0.0.0.0:5001/get?msg=olá")
    assert response.text == "Oizinho =)"
    
def teste3():
    response = requests.get("http://0.0.0.0:5001/get?msg=oi")
    assert response.text == "Oizinho =)"
    
def teste4():
    response = requests.get("http://0.0.0.0:5001/get?msg=tenho%2020%20anos")
    assert response.text == "Aproveite a juventude!"
    
def teste5():
    response = requests.get("http://0.0.0.0:5001/get?msg=3")
    assert response.text == "Fizz"
    
def teste6():
    response = requests.get("http://0.0.0.0:5001/get?msg=5")
    assert response.text == "Buzz"
    
def teste7():
    response = requests.get("http://0.0.0.0:5001/get?msg=15")
    assert response.text == "FizzBuzz"
